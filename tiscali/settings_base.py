from django.utils.translation import ugettext
import os


TIME_ZONE = 'Europe/Prague'

LANGUAGE_CODE = 'cs'

ADMINS = (
    ('Vladimir Kloucek', 'vlada.kloucek@gmail.com'),
)

MANAGERS = ADMINS

gettext = lambda s: s

PROJECT_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)

LANGUAGES = (
    ('cs', gettext('Czech')),
    ('en', gettext('English')),
)

LOCALE_PATHS = (
    os.path.join(PROJECT_ROOT, 'locale'),
)

SITE_ID = 1

MEDIA_URL = '/m/'

STATIC_URL = '/files/'


SECRET_KEY = 'aaa9m*0)27nqx7^osibji++()+$2g+3wwv_u_dkv+l=kq5n=k_3jy'

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/media/'

STATIC_URL = '/files/'
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, 'files'),
)

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)

AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.RemoteUserBackend',
        'django.contrib.auth.backends.ModelBackend',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'tiscali.urls'

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    "tiscali.context_processors.get_categories"
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.humanize',
    'sorl.thumbnail',
    'eshop'
)

DUMMY_IMAGES_SOURCE = 'http://placehold.it/500x500'

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from eshop.urls import eshop_urls

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       )

urlpatterns += eshop_urls

if settings.DEBUG:
    urlpatterns += patterns('',
                            (r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:],
                             'django.views.static.serve',
                             {'document_root': settings.MEDIA_ROOT,
                              'show_indexes': True}),
                            (r'^%s(?P<path>.*)$' % settings.STATIC_URL[1:],
                             'django.views.static.serve',
                             {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
                            )

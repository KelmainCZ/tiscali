# coding: utf-8
import eshop.models

def get_categories(request):
    return {
        'MAIN_CATEGORIES': eshop.models.Category.objects.filter(parent=None)
    }
# Django settings for nemocnice project.

import os

from settings_base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'idvideo',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'idvideo',
        'PASSWORD': '2--qabjP2kXWwvup',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
        'STORAGE_ENGINE': 'InnoDB'
    }
}

ALLOWED_HOSTS = ['http://idvideo.nebenet.cz/', 'idvideo.nebenet.cz', 'http://idvideo.cz', 'http://www.idvideo.cz', 'idvideo.cz']
#ALLOWED_HOSTS = []

PROJECT_ROOT = os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')
)
PROJECT_URL = 'idvideo.nebenet.cz'

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/m/'

STATIC_URL = '/files/'
#STATICFILES_DIRS = (
#    os.path.join(PROJECT_ROOT, 'files'),
#)
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'files')

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates'),
)

DEFAULT_CC_EMAIL = ['vlada.kloucek@gmail.com']

EMAIL_USE_TLS = False
EMAIL_HOST = 'localhost'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
MAXIMUM_MAILS_PER_COMMAND = 30
DEFAULT_FROM_EMAIL = 'info@idvideo.cz'

CONTACT_EMAIL = 'info@idvideo.cz'

ANONYMOUS_MAN_PHOTO_PATH = os.path.join(MEDIA_ROOT, 'man-lg.png')
ANONYMOUS_WOMAN_PHOTO_PATH = os.path.join(MEDIA_ROOT, 'woman-lg.png')

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.RemoteUserBackend',
    'django.contrib.auth.backends.ModelBackend',
)

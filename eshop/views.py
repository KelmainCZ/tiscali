# coding: utf-8
from django.views.generic import *
from django.shortcuts import get_object_or_404
from django.db.models import Q
import models

class ItemListView(ListView):
    template_name = 'eshop/category.html'
    model = models.Item
    paginate_by = 20

    def get_queryset(self):
        self.category = None
        if 'category' in self.kwargs:
            self.category = get_object_or_404(models.Category, slug=self.kwargs['category'])
            return models.Item.objects.filter(Q(category=self.category)|Q(category__parent=self.category)|Q(category__parent__parent=self.category))
        return models.Item.objects.all()

    def get_context_data(self, **kwargs):
        context = super(ItemListView, self).get_context_data(**kwargs)
        context['category'] = self.category
        return context

class ItemDetailView(DetailView):
    template_name = 'eshop/item.html'
    model = models.Item

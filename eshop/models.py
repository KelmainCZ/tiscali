# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.template.defaultfilters import slugify
from django.conf import settings

class Category(models.Model):
    parent = models.ForeignKey('eshop.Category', blank=True, null=True, verbose_name=u'Nadřazená kategorie', on_delete=models.SET_NULL, related_name='children')
    name = models.CharField(u'Název', blank=False, null=False, max_length=255)
    slug = models.SlugField(u'URL', blank=False, null=False, unique=True)

    class Meta:
        verbose_name = u'Kategorie'
        verbose_name_plural = u'Kategorie'
        ordering = ('name',)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        if self.parent:
            return reverse('category_items', args=(self.parent.slug, self.slug,))
        return reverse('main_category_items', args=(self.slug,))

class Item(models.Model):
    category = models.ForeignKey('eshop.Category', blank=True, null=True, verbose_name=u'Kategorie')
    import_id = models.CharField(u'Importované ID', blank=True, null=True, max_length=64)
    name = models.CharField(u'Název', blank=False, null=False, max_length=255)
    price = models.DecimalField(u'Cena', max_digits=12, decimal_places=2, blank=False, null=False)
    description = models.TextField(u'Popis', blank=True, null=True)
    image_url = models.URLField(u'URL obrázku', blank=True, null=True, max_length=512)

    class Meta:
        verbose_name = u'Produkt'
        verbose_name_plural = u'Produkty'
        ordering = ('name',)

    def __unicode__(self):
        return self.name

    def get_category_tree(self):
        to_ret = []
        category = self.category
        while category:
            to_ret.append(category)
            category = category.parent
        return reversed(to_ret)

    def get_image_url(self):
        return self.image_url if self.image_url else settings.DUMMY_IMAGES_SOURCE

    def get_slug(self):
        return slugify(self.name)

    def get_absolute_url(self):
        return reverse('item_detail', args=(self.category.slug, self.pk, self.get_slug()))

# coding: utf-8
from django.contrib import admin
from django.contrib.admin import ModelAdmin

import models

class CategoryAdmin(ModelAdmin):
    pass

admin.site.register(models.Category, CategoryAdmin)

class ItemAdmin(ModelAdmin):
    pass

admin.site.register(models.Item, ItemAdmin)

# coding: utf-8
from django.conf.urls import patterns, url
from views import *

eshop_urls = patterns(
    '',
    url(r'^eshop/$', ItemListView.as_view(), name="items_list"),
    url(r'^eshop/(?P<category>[\w-]+)/$', ItemListView.as_view(), name="main_category_items"),
    url(r'^eshop/(?P<category>[\w-]+)/(?P<pk>\d+)-(.*)/$', ItemDetailView.as_view(), name="item_detail"),
    url(r'^eshop/(?P<parent_category>[\w-]+)/(?P<category>[\w-]+)/$', ItemListView.as_view(), name="category_items"),
)

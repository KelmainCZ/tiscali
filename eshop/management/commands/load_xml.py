# coding: utf-8
import os

from django.core.management.base import BaseCommand
from django.conf import settings
from django.template.defaultfilters import slugify
from decimal import Decimal
from bs4 import BeautifulSoup

from eshop.models import Category, Item

def parse_category(category_string):
    categories_list = category_string.split('|')
    return [x.strip() for x in categories_list]

class Command(BaseCommand):
    help = "Import data from XML to DB models"

    def handle(self, *args, **options):
        xml_file = open(os.path.join(settings.MEDIA_ROOT, 'shop.xml'), 'r')
        soup = BeautifulSoup(xml_file, 'xml')
        for shopitem in soup.find_all('SHOPITEM'):

            #vytvareni kategorii
            categories = parse_category(shopitem.CATEGORYTEXT.string) #parsuje na strom kategorií
            last_category = None
            for category in categories:
                if Category.objects.filter(slug=slugify(category)).count()>0:
                    #kategorie se timhle slugem uz existuje
                    category_instance = Category.objects.get(slug=slugify(category))
                    if category_instance.parent != last_category:
                        #kategorie s timhle slugem ma jineho parenta, vytvor novou kategorii stejneho jmena se slugem slozenym z predka
                        try:
                            category_instance = Category.objects.create(name=category, slug=last_category.slug+'-'+slugify(category))
                        except:
                            category_instance = Category.objects.get(slug=last_category.slug+'-'+slugify(category))
                else:
                    category_instance = Category.objects.create(name=category, slug=slugify(category))
                category_instance.parent = last_category
                category_instance.save()
                last_category = category_instance

            #vytvoreni instance produktu
            item = Item()
            if Item.objects.filter(import_id=shopitem.ITEM_ID.string).count()>0:
                item = Item.objects.get(import_id=shopitem.ITEM_ID.string)
            item.category = last_category
            item.import_id = shopitem.ITEM_ID.string
            item.name = shopitem.PRODUCT.string
            item.description = shopitem.DESCRIPTION.string
            item.price = Decimal(shopitem.PRICE_VAT.string)
            try:
                item.image_url = shopitem.IMGURL.string
            except:
                pass
            item.save()
